cmake_minimum_required(VERSION 3.10)

project("FFmpeg Qt Frontend" LANGUAGES CXX)

set(DEFAULT_SOURCES "")
set(DEFAULT_TARGET "ffmpeg_frontend")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "bin")

add_subdirectory(ffmpeg_wrapper)

add_subdirectory(src)

add_executable(${DEFAULT_TARGET} ${DEFAULT_SOURCES})
target_link_libraries(${DEFAULT_TARGET} FFmpegCppWrapper)

set_target_properties(${DEFAULT_TARGET} PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED YES
    INCLUDE_DIRECTORIES "${CMAKE_CURRENT_SOURCE_DIR}/src"
)
target_compile_options(${DEFAULT_TARGET} PRIVATE -Wall -Wextra -pedantic)

