/// @file main.cpp
#include <map>
#include <string>
#include <vector>

#include <cstdio>

#include "ffmpeg_wrapper/probe.hpp"
#include "ffmpeg_wrapper/about.hpp"

int main() {
    std::vector<std::string> fileList = {"file_a", "file_b", "file_c"};
    std::map<std::string, json> fileData;

    int status = scanFiles(fileList, fileData);

    printf("scanFile(): %d\n", status);

    for(auto pair : fileData) {
        printf("Key: '%s', Value: '%s'\n", pair.first.c_str(), pair.second.dump().c_str());
    }
    return 0;
}
