/**
 * @file probe.hpp
 * @brief Probes files for video information.
 */
#ifndef FFMPEG_WRAPPER_PROBE_HPP
#define FFMPEG_WRAPPER_PROBE_HPP

#include <map>
#include <string>
#include <vector>

#include "utils/json.hpp"
using json = nlohmann::json;

#include "utils/process.hpp"

int scanFiles(const std::vector<std::string> &files,
              std::map<std::string, json> &data);

#endif
