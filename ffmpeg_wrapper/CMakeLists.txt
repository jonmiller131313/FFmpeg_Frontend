cmake_minimum_required(VERSION 3.10)

project("FFmpeg C++ Wrapper Library" VERSION 0.0.1 LANGUAGES CXX)
set(DEFAULT_LIBRARY "FFmpegCppWrapper")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "static_libs")

message("PROJECT_VERSION: ${PROJECT_VERSION}")

set(LIBRARY_SOURCES
    probe.cpp
)
configure_file(about.hpp.in ${CMAKE_CURRENT_SOURCE_DIR}/about.hpp)

add_subdirectory(utils)

add_library(${DEFAULT_LIBRARY} STATIC ${LIBRARY_SOURCES})

set_target_properties(${DEFAULT_LIBRARY} PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED YES
    INCLUDE_DIRECTORIES ${CMAKE_CURRENT_SOURCE_DIR}
    INTERFACE_INCLUDE_DIRECTORIES ${CMAKE_CURRENT_SOURCE_DIR}/.. # So that you have to #include "ffmpeg_wrapper/..."
)
target_compile_options(${DEFAULT_LIBRARY} PUBLIC -Wall -Wextra -pedantic)
