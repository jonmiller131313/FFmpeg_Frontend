/**
 * @file process.hpp
 * @brief Sub-process utility functions.
 */
#ifndef FFMPEG_WRAPPER_PROCESS_HPP
#define FFMPEG_WRAPPER_PROCESS_HPP

#include <stdexcept>
#include <string>

#include <cstdio>

/**
 * @brief Executes a command and returns the result from `stdout`.
 *
 * @param cmd The command to execute.
 *
 * @return The output of `cmd` from `stdout`.
 *
 * @throw std::runtime_error if `popen()` fails.
 */
const std::string exec(const std::string &cmd);

#endif
