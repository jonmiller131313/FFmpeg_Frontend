/// @file process.cpp
#include "process.hpp"

const std::string exec(const std::string& cmd) {
    char buffer[128];
    std::string result = "";

    FILE *pipe = popen(cmd.c_str(), "r");

    if(!pipe)
        throw std::runtime_error("popen() failed!");

    while(fgets(buffer, sizeof(buffer), pipe) != nullptr) {
        result += buffer;
    }

    pclose(pipe);

    return result;
}


