/**
 * @file about.hpp
 * @brief Information about the FFmpegCppWrapper library.
 */
#ifndef FFMPEG_WRAPPER_ABOUT_HPP
#define FFMPEG_WRAPPER_ABOUT_HPP

#define FFMPEG_CPP_WRAPPER_VERSION "0.0.1"
#define FFMPEG_CPP_WRAPPER_VERSION_MAJOR 0
#define FFMPEG_CPP_WRAPPER_VERSION_MINOR 0
#define FFMPEG_CPP_WRAPPER_VERSION_PATCH 1

#endif
