/// @file probe.cpp
#include "probe.hpp"

int scanFiles(const std::vector<std::string> &files,
              std::map<std::string, json> &data) {
    int count = 0;
    for(const std::string &file : files) {
        std::string output = exec("echo 'FILE = " + file + "'");
        json jsonData = { {"data", output} };
        data[file] = jsonData;
        count++;
    }

    return count;
}
